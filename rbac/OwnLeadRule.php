<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnLeadRule extends Rule
{
	public $name = 'ownLeadRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			//echo '<script>window.alert("Current User is:'.$user.' and Owner of Lead is:'.$params['user']->id.'")</script>';
			return isset($params) ? $params['user']->id == $user : false;
		}
		return false;
	}
}