<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?php if (\Yii::$app->user->can('createUser') ){?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            //'password',
            //'auth_key',
            'firstname',
            'lastname',
            'email:email',
            'phone',
            //'created_at',
            [
                //Created at of the user
                'label' => $model->attributeLabels()['created_at'],
                'value' => date('Y-m-d H:i A', $model->created_at),
            ],
            [
                //Updated at of the user
                'label' => $model->attributeLabels()['updated_at'],
                'value' => date('Y-m-d H:i A', $model->updated_at),
            ],
            [ // Who Created the User
				'label' => $model->attributeLabels()['created_by'],
				'format' => 'html',
				'value' => Html::a($model->createdBy->fullname, 
					['user/view', 'id' => $model->createdBy->id]),	
			],
            [ // Who updated the User
				'label' => $model->attributeLabels()['updated_by'],
				'format' => 'html',
				'value' => Html::a($model->updatedBy->fullname, 
					['user/view', 'id' => $model->updatedBy->id]),	
			],
        ],
    ]) ?>

</div>
